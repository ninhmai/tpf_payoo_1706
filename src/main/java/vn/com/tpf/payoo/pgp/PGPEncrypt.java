package vn.com.tpf.payoo.pgp;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;

public class PGPEncrypt {
    public String encryptMethod(String encryptData, String privateKeyPath, String publicKeyPath, String preshareKey) throws Exception, FileNotFoundException {

        //final String privateKeyPath = System.getProperty("user.dir") + "/pgp/tpf_fpt-sec.asc";
        //final String publicKeyPath = System.getProperty("user.dir") + "/pgp/frt_pub.asc";
        PGPHelper.init(
                privateKeyPath,
                publicKeyPath,
                preshareKey);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        PGPHelper.getInstance().encryptAndSign(encryptData.getBytes(), out);
        return  out.toString();

    }
}
