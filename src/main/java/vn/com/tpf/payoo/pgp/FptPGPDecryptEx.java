package vn.com.tpf.payoo.pgp;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;

public class FptPGPDecryptEx {
    public static void main(String[] args) throws Exception, FileNotFoundException {

        

        PGPHelper.init(

        		PgpConstants.tpfPrivateKeyPath,
        		PgpConstants.fptPublicKeyPath,
                "123456");
        




        String encryptData1 = "-----BEGIN PGP MESSAGE-----\n" +
                "Version: BCPG v1.46\n" +
                "\n" +
                "hQEMA2xNCMjf2BpqAQf/eoJWkkONz8QSI91TH1Yf9kv4nZrOS8MJXiTNqqCtxEwP\n" +
                "sok/2Eu2gI+ukbdoT5phI8EmyoyEd9rOM6gOJaA7BEbxdCF08uBDkcoZlD8sanfx\n" +
                "7Q0Sy/Blt9EeFagascOK9Xuj3zWQf9LgcTT1OSHDfFyhSivqRVrqErDMbkeqV5Vo\n" +
                "E79kf8eWLdH5yoUWVO5pBuKzVGxJqbcdimyuFxEuNNfp4miuRiTsc/4RMkGyhY6U\n" +
                "6+a1VF6mdL32rzw+25caGI3gqBi2OO47Z0LAcqbGMnzPY8l7CDuGrbJQe44PYdUX\n" +
                "Mv7icvHJrnxgqIuEbXxFU2GBWRUtOKwFQ/b0DzMCHcnAtOMcpEAFCGCayfr9FrUl\n" +
                "4vi5iIO1anPbUnymQ32mFRpDdTs/OC0PTem83R90XfVmsyuaTD071IQvab9MeFyU\n" +
                "ZSZuZVdVb4dPMRpM1IUyjfKu6JlDtacoVQsd4090UwYbhmlSBGx5jHj0in6lijnm\n" +
                "USY/kl8Q9Xd5MWOmZmpi18d7quS6SqWU7sl1NCv73gP3ylV07ePKxGrnUR11rrPE\n" +
                "N8DcIh5nI1NrMA4bYr89lWgpPY4sVhJQEtaW4tdsZs3gjrvp7KM4aCGA2UVV+HmW\n" +
                "QLDFgSUu70Xv69mP/m6QBdqzPMUV79Sy+J8WTTC38+6P4AbhK+BJm2zAfJm9y29/\n" +
                "bxx2Wv89IkpzCF5fsyJ765qbT8jgFZ6aDrMlRqFU+0RLTpSteGlSXufDJM/SFAjb\n" +
                "IXB5asYGaDBtTQm3TNjdP7amW1vm4NMloHdUmQ3PIFU6sT0N31V05rfAzCoEbRRv\n" +
                "pvw63VIuGA/0BmRQmGN7rSJLxPoXmg==\n" +
                "=UHOZ\n" +
                "-----END PGP MESSAGE-----";

        System.out.println("Decrypted data process");
        System.out.println(encryptData1);
        //Create byte array output stream to store decrypted data
        ByteArrayOutputStream desStream = new ByteArrayOutputStream();
        //Decrypted data and send to desStream with input data is encryptedData
        PGPHelper.getInstance().decryptAndVerifySignature(encryptData1.getBytes(), desStream);
        //print decrypted data
        System.out.println(desStream.toString());

    }
}
