package vn.com.tpf.payoo.pgp;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;

public class PGPDecrypt {
    public String decryptMethod(String encryptData, String privateKeyPath, String publicKeyPath, String preshareKey) throws Exception, FileNotFoundException {

        /*final String privateKeyPath = System.getProperty("user.dir") + "/pgp/tpf_fpt-sec.asc";
        final String publicKeyPath = System.getProperty("user.dir") + "/pgp/frt_pub.asc";*/

        PGPHelper.init(
                privateKeyPath,
                publicKeyPath,
                preshareKey);


        ByteArrayOutputStream desStream = new ByteArrayOutputStream();

        PGPHelper.getInstance().decryptAndVerifySignature(encryptData.getBytes(), desStream);

        return  desStream.toString();

    }
}
