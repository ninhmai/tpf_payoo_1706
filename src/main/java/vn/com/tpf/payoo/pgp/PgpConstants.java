package vn.com.tpf.payoo.pgp;

public class PgpConstants {
    /*TPF key*/
    public static final String tpfPrivateKeyPath = System.getProperty("user.dir") + "/pgp/tpf_fpt-sec.asc";
    public static final String tpfPublicKeyPath = System.getProperty("user.dir") + "/pgp/tpf_fpt-pub.asc";
    public static final String tpfPreshareKey = "123456";

    /*FPT keys*/
    public static final String fptPublicKeyPath = System.getProperty("user.dir") + "/pgp/frt_pub.asc";
    public static final String fptPrivateKeyPath = System.getProperty("user.dir") + "/pgp/frt_priv.asc";
    public static final String fptPreShareKey = "123456a@";

    /*Momo keys*/
    public static final String momoPublicKeyPath = System.getProperty("user.dir") + "/pgp/momo_pub_20190227.asc";

}
