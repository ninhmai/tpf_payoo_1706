package vn.com.tpf.payoo.pgp;

import static vn.com.tpf.payoo.pgp.PgpConstants.*;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;



public class FptPGPEncryptEx {
    public static void main(String[] args) throws Exception, FileNotFoundException {


        PGPHelper.init(
                fptPrivateKeyPath,
                tpfPublicKeyPath,
                fptPreShareKey);

        String enctypt = "{\n" +
                "  \"loanId\":5019211,\n" +
                "  \"transactionId\": \"PY2019032500016\",\n" +
                "  \"createDate\": \"1553494218128\",\n" +
                "  \"loanAccountNo\": \"CA000005019210\",\n" +
                "  \"identificationNumber\": \"381411826\",\n" +
                "  \"amount\": 50000\n" +
                "}";
        //Create byte array output stream to store encrypted date
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        //Encrypt data and send out
        //.getInstance().encryptAndSign(object.toString().getBytes(), out);
        PGPHelper.getInstance().encryptAndSign(enctypt.getBytes(), out);
        //Print encrypted data
        String encryptData = out.toString();

        System.out.println("Data after encrypted \n" + encryptData);


    }
}
