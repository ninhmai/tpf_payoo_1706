package vn.com.tpf.payoo.error;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
@ControllerAdvice
public class CustomerRestExceptionHandler {
    // Add an exception handler for CustomerNotFoundException

    
    
    @ExceptionHandler(value = PayooDoubleException.class )
	public ResponseEntity<?> exceptionPayooDoubleException(Exception ex, WebRequest request) {
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
				.body(new CustomerErrorResponse(
	                    11,
	                    "Duplicate Unique Key",
	                    new java.util.Date()));
	}
    
    
    @ExceptionHandler(value = CustomerNotFoundException.class )
   	public ResponseEntity<?> exceptionCustomerNotFoundException(Exception ex, WebRequest request) {
   		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
   				.body(new CustomerErrorResponse(
   	                    0,
   	                 ex.getMessage(),
   	                    new java.util.Date()));
   	}
 
    
    @ExceptionHandler(value = { Exception.class })
	public ResponseEntity<?> exception(Exception ex, WebRequest request) {
    	System.out.println(ex.getMessage());
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
				.body(CustomerErrorResponse.builder().status(0).message("Loi Du Lieu").build());
	}
}
