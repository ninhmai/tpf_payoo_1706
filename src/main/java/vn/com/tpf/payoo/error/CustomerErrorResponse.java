package vn.com.tpf.payoo.error;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Builder;

import java.util.Date;

@Builder
public class CustomerErrorResponse {
	private int status;
    private String message;
    
    
    @Temporal(TemporalType.TIMESTAMP)
    @Builder.Default
    private Date timeStamp = new Date();

    public CustomerErrorResponse(int status, String error, String message) {

    }

    public CustomerErrorResponse(int status, String message, Date timeStamp) {
        this.status = status;
        this.message = message;
        this.timeStamp = timeStamp;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    @Override
    public String toString() {
        return "{\n" +
                "  \"status\": \"" + status + "\",\n" +
                "  \"message\": \"" + message + "\",\n" +
                "  \"timeStamp\": \"" + timeStamp + "\"\n" +
                '}';
    }
}
