package vn.com.tpf.payoo.payoo.dao;

import java.io.Serializable;
import java.util.Date;

public class ResponseObjectModel<T> implements Serializable {

    public static final ResponseObjectModel SUCCESS_WITHOUT_DATA = new ResponseObjectModel(null);
    public static final ResponseObjectModel FAILURE = new ResponseObjectModel("2", "500", new Date());
    public static final ResponseObjectModel NOT_FOUND = new ResponseObjectModel("3", "404", new Date());

//    private boolean success;

    private String status;
    private String message;
    private Date timeStamp;

    private T data;

    public ResponseObjectModel() {
        this.setStatus("0");
        this.setMessage("OK");
    }

    public ResponseObjectModel(T data) {
        this();
        this.data = data;
    }

    public ResponseObjectModel(String status, String message, Date timeStamp) {
        this(status, message, timeStamp, null);
    }

    public ResponseObjectModel(String status, String message, Date timeStamp, T data) {
        this.setStatus(status);
        this.setMessage(message);
        this.setTimeStamp(timeStamp);
        this.setData(data);
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

//    public boolean isSuccess() {
//        return success;
//    }
//
//    public void setSuccess(boolean success) {
//        this.success = success;
//    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setMessage(String format, String... params) {
        if (params != null && params.length > 0) {
            this.message = String.format(format, (Object[]) params);
        }
    }

    public void setFailMessage(String format, String... params) {
        this.setStatus("1");
        this.setTimeStamp(new Date());
        this.setData(null);
        if (params != null && params.length > 0) {
            this.message = String.format(format, (Object[]) params);
        } else {
            this.message = format;
        }
    }

    public static ResponseObjectModel failResponse(String message) {
        return new ResponseObjectModel("1", message, new Date());
    }
}

