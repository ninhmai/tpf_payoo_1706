package vn.com.tpf.payoo.payoo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import vn.com.tpf.payoo.payoo.entity.FicoCustomer;

import java.util.List;
import java.util.Optional;


public interface FicoCustomerDAO extends JpaRepository<FicoCustomer,Long> {
    List<FicoCustomer> findByCustomerNameContaining(String customerName);
    List<FicoCustomer> findByIdentificationNumber(String identificationNumber);
    List<FicoCustomer> findByLoanAccountNo(String loanAccountNo);
    FicoCustomer findByLoanId(Long loanId);
}
