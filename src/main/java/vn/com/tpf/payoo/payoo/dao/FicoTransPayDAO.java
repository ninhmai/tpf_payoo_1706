package vn.com.tpf.payoo.payoo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import vn.com.tpf.payoo.payoo.entity.FicoTransPay;
import org.springframework.data.repository.CrudRepository;

@Transactional
public interface FicoTransPayDAO extends CrudRepository<FicoTransPay,Long> {
    FicoTransPay findByTransactionId(String id);

    @Procedure(name = "sp_settle_cancel1")
        public void sp_settle_cancel1(@Param("p_trans_code") String p_trans_code,@Param("p_reason") String p_reason);

}
