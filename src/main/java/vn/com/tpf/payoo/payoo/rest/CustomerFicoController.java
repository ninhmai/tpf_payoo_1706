package vn.com.tpf.payoo.payoo.rest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import vn.com.tpf.payoo.error.CustomerErrorResponse;
import vn.com.tpf.payoo.error.CustomerNotFoundException;
import vn.com.tpf.payoo.error.PayooDoubleException;
import vn.com.tpf.payoo.payoo.dao.FicoCustomerDAO;
import vn.com.tpf.payoo.payoo.dao.FicoTransPayDAO;
import vn.com.tpf.payoo.payoo.dao.ResponseObjectModel;
import vn.com.tpf.payoo.payoo.entity.FicoCustomer;
import vn.com.tpf.payoo.payoo.entity.FicoTransPay;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.*;
import javax.validation.Valid;





@RestController
//@RequestMapping("/api_dev")
public class CustomerFicoController {
	@Autowired
    private FicoCustomerDAO ficoCustomerDAO;
	@Autowired
    private FicoTransPayDAO ficoTransPayDAO;

//    private static EntityManagerFactory entityManagerFactory =
//            Persistence.createEntityManagerFactory("org.hibernate.tutorial.jpa");

    public CustomerFicoController(FicoCustomerDAO ficoCustomerDAO, FicoTransPayDAO ficoTransPayDAO) {
        this.ficoCustomerDAO = ficoCustomerDAO;
        this.ficoTransPayDAO = ficoTransPayDAO;
    }

    /*List all customers in Fico*/
    @GetMapping("/customers_fico")
    public List<FicoCustomer> getAllCustomer() {
        return ficoCustomerDAO.findAll();
    }

    /*Get customer by id / loanId*/
    @PostMapping("/customers_fico/{id}")
    public FicoCustomer getCustomer(@PathVariable long id) {
        System.out.println(ficoCustomerDAO.findByLoanId(id));
        return ficoCustomerDAO.findByLoanId(id);
    }

    /*Get customer by Info including Name, Id, AccNo*/
    @PostMapping(value = "/customers_fico/")
    public List<FicoCustomer> getCustomerCustomerName(@RequestParam(value = "Info",required = true) String info,
                                                      @RequestParam(value="Value",required = true) String value) throws Exception {
        List<FicoCustomer> ficoCustomerList = null;
        if(info.isEmpty() || value.isEmpty()) {
    		throw new  Exception();
    	}
//        System.out.println(info + value);
        if(info.equals("Name")) {
            ficoCustomerList = ficoCustomerDAO.findByCustomerNameContaining(value.toUpperCase());
        }
        if(info.equals("Id")) {
            ficoCustomerList = ficoCustomerDAO.findByIdentificationNumber(value);
        }
        if(info.equals("AccNo")) {
            ficoCustomerList = ficoCustomerDAO.findByLoanAccountNo(value);
        }

        ficoCustomerList=ficoCustomerList.stream().filter(p->p.getLoanActive()==true).collect(Collectors.toList());

//        System.out.println(ficoCustomerList.size());
        if(ficoCustomerList.size() == 0) {
            throw new CustomerNotFoundException("not exist");
        }

        return ficoCustomerList;
    }


    /*Payment*/
    @PostMapping("/customers_fico_pay")
    public CustomerErrorResponse addCustomer(@RequestBody @Valid FicoTransPay ficoTransPay) throws Exception{
//        CustomerErrorResponse customerErrorResponse = null;
        //customerErrorResponse = new CustomerErrorResponse(1,"Success",new java.util.Date());
        try{
            ficoTransPayDAO.save(ficoTransPay);
            return new CustomerErrorResponse(1,"Success",new java.util.Date());
        }
        catch (Exception e) {
                System.out.println(e.getMessage());
        	 throw new PayooDoubleException();
        }
    }

    /*Check Payment*/
    @PostMapping("/customers_fico_pay/")
    public FicoTransPay getPayment(@RequestParam(value="Value" ,required = true) String value) throws Exception {
//        System.out.println("Test" + value);
//        System.out.println(ficoTransPayDAO.findByTransactionId(value));
    	if(value.isEmpty()) {
    		throw new  Exception();
    	}

        FicoTransPay ficoTransPay = ficoTransPayDAO.findByTransactionId(value);
        if(ficoTransPay == null) {
            throw new CustomerNotFoundException("not exist");
        }
        return ficoTransPayDAO.findByTransactionId(value);
    }



//    //  -------------MOMMO--------------
//
//    @GetMapping(value = "/customers")
//    public ResponseObjectModel<List<FicoCustomer>> customers(@RequestParam(value = "search_value",required = true) String search_value) throws Exception {
//        ResponseObjectModel responseObjectModel = new ResponseObjectModel();
//        try{
//            List<FicoCustomer> ficoCustomerList = null;
//            if(search_value.isEmpty()) {
//                throw new  Exception();
//            }
//            if(isValidIdNumer(search_value)) {
//                ficoCustomerList = ficoCustomerDAO.findByIdentificationNumber(search_value);
//            }else{
//                ficoCustomerList = ficoCustomerDAO.findByLoanAccountNo(search_value);
//            }
//            if(ficoCustomerList.size() == 0) {
//                responseObjectModel.setStatus("1");
//                responseObjectModel.setMessage("not exist");
//                responseObjectModel.setTimeStamp(new Date());
//            }else {
//                responseObjectModel.setStatus("0");
//                responseObjectModel.setMessage("Success");
//                responseObjectModel.setTimeStamp(new Date());
//                responseObjectModel.setData(ficoCustomerList);
//            }
//        }
//        catch (Exception e) {
//            responseObjectModel.setFailMessage("Error: " + e.getMessage());
//        }
//        return responseObjectModel;
//    }
//
//    @PostMapping("/customers_pay")
//    public ResponseObjectModel customers_pay(@RequestBody @Valid FicoTransPay ficoTransPay) throws Exception{
//        ResponseObjectModel responseObjectModel = new ResponseObjectModel();
//        try{
//            ficoTransPayDAO.save(ficoTransPay);
//            responseObjectModel.setStatus("0");
//            responseObjectModel.setMessage("Success");
//            responseObjectModel.setTimeStamp(new Date());
//        }
//        catch (Exception e) {
//            responseObjectModel.setStatus("1");
//            responseObjectModel.setMessage("not exist");
//            responseObjectModel.setTimeStamp(new Date());
//        }
//        return responseObjectModel;
//    }
//
//    @GetMapping("/customers_pay")
//    public ResponseObjectModel<FicoTransPay> customers_pay(@RequestParam(value="transactionId" ,required = true) String transactionId) throws Exception {
//        ResponseObjectModel responseObjectModel = new ResponseObjectModel();
//        try{
//            if(transactionId.isEmpty()) {
//                throw new  Exception();
//            }
//            FicoTransPay ficoTransPay = ficoTransPayDAO.findByTransactionId(transactionId);
//            if(ficoTransPay == null) {
//                responseObjectModel.setStatus("1");
//                responseObjectModel.setMessage("not exist");
//                responseObjectModel.setTimeStamp(new Date());
//            }else{
//                responseObjectModel.setStatus("0");
//                responseObjectModel.setMessage("Success");
//                responseObjectModel.setTimeStamp(new Date());
//                responseObjectModel.setData(ficoTransPay);
//            }
//        }
//        catch (Exception e) {
//            responseObjectModel.setFailMessage("Error: " + e.getMessage());
//        }
//        return responseObjectModel;
//    }
//
//    private static boolean isValidIdNumer(String strNum) {
//        if(!strNum.matches("^[0-9]+$") && strNum.length()!=9 && strNum.length()!=12)
//        {
//            return false;
//        }
//        return true;
//    }
//
//    @GetMapping("/customers_cancel")
//    public ResponseObjectModel<FicoTransPay> customers_cancel(@RequestParam(value="transactionId" ,required = true) String transactionId) throws Exception {
//        ResponseObjectModel responseObjectModel = new ResponseObjectModel();
//        try{
//            if(transactionId.isEmpty()) {
//                throw new  Exception();
//            }
//
//
//            ficoTransPayDAO.sp_settle_cancel1(transactionId, "test");
//            responseObjectModel.setStatus("00");
//            responseObjectModel.setMessage("success");
//            responseObjectModel.setTimeStamp(new Date());
//
//        }
//        catch (Exception e) {
//            responseObjectModel.setFailMessage("Error: " + e.getMessage());
//        }
//        return responseObjectModel;
//    }

}
