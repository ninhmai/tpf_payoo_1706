package vn.com.tpf.payoo.payoo.entity;

import org.apache.tomcat.jni.Local;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;

//@NamedStoredProcedureQueries({
//        @NamedStoredProcedureQuery(
//                name = "FicoTransPay.cancel",
//                procedureName = "payoo.sp_settle_cancel1",
//                parameters = {
//                        @StoredProcedureParameter(name = "p_trans_code", mode = ParameterMode.IN, type = String.class),
//                        @StoredProcedureParameter(name = "p_reason", mode = ParameterMode.IN, type = String.class)
//                })
//})




@Entity
@Table(name = "fico_trans_pay", schema = "payoo")
@NamedStoredProcedureQueries({
        @NamedStoredProcedureQuery(
                name = "sp_settle_cancel1",
                procedureName = "sp_settle_cancel1",
                parameters = {
                        @StoredProcedureParameter(
                                mode = ParameterMode.IN,
                                name = "p_trans_code",
                                type = String.class),
                        @StoredProcedureParameter(
                                mode = ParameterMode.IN,
                                name = "p_reason",
                                type = String.class)
                }
        )
})
public class FicoTransPay implements Serializable {
    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE)
    @Column(name="id")
    private long id;

    @Column(name="transaction_id", unique = true, nullable = false)
    private String transactionId;

    @Column(name="loanid", nullable = false)
    private long loanId;

    @Column(name="create_date")
    private Timestamp createDate;

    @Column(name="loan_account_no")
    private String loanAccountNo;

    @Column(name="identification_number")
    private String identificationNumber;

    @Column(name="amount")
    @Min(0)
    @NotNull
    private long amount;

    public FicoTransPay() {
    }

    public FicoTransPay(Timestamp createDate, String loanAccountNo, String identificationNumber, long amount) {
        this.createDate = createDate;
        this.loanAccountNo = loanAccountNo;
        this.identificationNumber = identificationNumber;
        this.amount = amount;
    }

    public long getLoanId() {
        return loanId;
    }

    public void setLoanId(long loanId) {
        this.loanId = loanId;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    public String getLoanAccountNo() {
        return loanAccountNo;
    }

    public void setLoanAccountNo(String loanAccountNo) {
        this.loanAccountNo = loanAccountNo;
    }

    public String getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public FicoTransPay(String transactionId, long loanId, Timestamp createDate, String loanAccountNo, String identificationNumber, long amount) {
        this.transactionId = transactionId;
        this.loanId = loanId;
        this.createDate = createDate;
        this.loanAccountNo = loanAccountNo;
        this.identificationNumber = identificationNumber;
        this.amount = amount;
    }
}
