package vn.com.tpf.payoo.config;

import org.springframework.boot.actuate.autoconfigure.security.servlet.EndpointRequest;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
//import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;

@Configuration
public class SecurityConfig extends ResourceServerConfigurerAdapter {

	private final String resourceId = "tpf-service-payoo";

	@Override
	public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
		// TODO Auto-generated method stub
		resources.resourceId(resourceId);
		super.configure(resources);
	}

	@Override
	public void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().requestMatchers(EndpointRequest.toAnyEndpoint()).permitAll()
				.antMatchers("/**").access("#oauth2.hasScope('tpf-service-payoo') or #oauth2.hasScope('tpf-service-repayment')")
				.anyRequest().authenticated();
//
//		http.authorizeRequests().antMatchers(HttpMethod.POST, "/**").permitAll();
//		http.authorizeRequests().antMatchers(HttpMethod.GET, "/**").permitAll();
	}
}
